require 'rails_helper'

RSpec.configure do |config|
  config.include DeviseRequestSpecHelpers, type: :request
end

RSpec.describe ArtistsController, type: :controller do
  def user
    User.create(
      name: 'AdminTest',
      email: 'admin_test@example.ca',
      password: '123456',
      admin: true
    )
  end

  def artist
    Artist.create(
      name: 'Bono',
      user_id: User.last
    )
  end

  describe '#new' do
    context 'without signed in user' do
      it 'redirect the user to devise/sessions#new page' do
        get :new
        expect(response).to redirect_to(new_user_session_path)
      end
    end

    context  'with signed in user / admin' do
      before do
        sign_in(user)
      end

      it 'renders the new template' do
        get :new
        expect(response).to render_template(:new)
      end

      it 'sets an instance variable with a new artist' do
        get :new
        expect(assigns(:artist)).to(be_a_new(Artist))
      end
    end
  end

  describe '#create' do
    def valid_request
      post :create, params: {
        artist: :artist
      }
    end

    context 'with user not signed in' do
      it 'redirects to the new session path' do
        valid_request
        expect(response).to redirect_to(new_user_session_path)
      end
    end

    context 'with user signed in' do
      before do
        sign_in(user)
      end

      context 'with valid parameters' do

        it 'creates a new artist in the database' do
          count_before = Artist.count
          valid_request
          count_after = Artist.count

          expect(count_before).to eq(count_after - 1)
        end

        it 'redirects to the show page of that artist' do
          valid_request
          expect(response).to redirect_to(artist_path(Artist.last))
        end

        it 'sets a flash message' do
          valid_request
          expect(flash[:notice]).to be
        end

        it 'associates the artist with the signed in user' do
          valid_request
          # byebug
          expect(Artist.last.user).to eq(user)
        end
      end

      context 'with invalid parameters' do
        def invalid_request
          post :create, params: {
            artist: FactoryBot.attributes_for(:artist).merge({name: nil})
          }
        end

        it 'doesn\'t create a artist in the database' do
          count_before = Artist.count
          invalid_request
          count_after = Artist.count

          expect(count_before).to eq(count_after)
        end

        it 'renders the new template' do
          invalid_request
          expect(response).to render_template(:new)
        end
      end
    end
  end

  describe '#show' do
    def artist
      Artist.create(
        name: 'Bono',
        user_id: User.last
      )
    end
    def valid_request
      get :show, params: { id: artist.id }
    end

    it 'renders the show template' do
      valid_request
      # THEN: It renders the show template
      expect(response).to render_template(:show)
    end
  end

  describe '#edit' do
    context 'with no user signed in' do
      it 'redirects the user to the new sessions path' do
        get :edit, params: {id: artist.id}
        expect(response).to redirect_to(new_user_session_path)
      end
    end

  end

  describe '#update' do
    context 'with valid parameters' do
      before do
        patch :update, params: {
          id: artist.id,
          artist: { name: 'abc' }
        }
      end

      it 'saves the changes' do
        # `reload` is an instance method of active record instances.
        # It can be used to refresh the data in a model instance if
        # it changes in the db by some other means.
        expect(artist.reload.name).to eq('abc')
      end

      it 'redirects to the artist show page' do
        expect(response).to redirect_to(artist_path(artist))
      end
    end

    context 'with invalid parameters' do
      it 'renders the edit template' do
        patch :update, params: {
          id: artist.id,
          artist: { name: '' }
        }
        expect(response).to render_template(:edit)
      end
    end
  end

end
