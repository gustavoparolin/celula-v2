FactoryBot.define do
  factory :artist do
    user nil
    name "MyString"
    image "MyString"
    biography "MyText"
    account_holder "MyString"
    bank "MyString"
    branch "MyString"
    account "MyString"
    account_type "MyString"
  end
end
