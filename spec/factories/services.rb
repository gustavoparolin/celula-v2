FactoryBot.define do
  factory :service do
    user nil
    provider "MyString"
    uid "MyString"
    access_token "MyString"
    access_token_secret "MyString"
    refresh_token "MyString"
    expires_at "2018-04-01 09:19:52"
    auth "MyText"
  end
end
