FactoryBot.define do
  factory :user do
    name { Faker::Name.name }
    sequence(:email) { |n| Faker::Internet.email.sub('@', "-#{n}@") }
    password {'123456'}
    admin {true}
  end
end
