FactoryBot.define do
  factory :announcement do
    published_at "2018-04-01 09:19:51"
    announcement_type "MyString"
    name "MyString"
    description "MyText"
  end
end
