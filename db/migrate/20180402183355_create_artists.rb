class CreateArtists < ActiveRecord::Migration[5.1]
  def change
    create_table :artists do |t|
      t.references :user, foreign_key: true
      t.string :name
      t.string :image
      t.text :biography
      t.string :account_holder
      t.string :bank
      t.string :branch
      t.string :account
      t.string :account_type

      t.timestamps

    end

    add_index :artists, :name, unique: true
  end
end
