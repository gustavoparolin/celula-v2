PASSWORD_ADMIN = 'V@nc0uv3r'
PASSWORD = '123456'

# Delete all lines of this tables
Announcement.destroy_all
Notification.destroy_all
Service.destroy_all
Artist.destroy_all
User.destroy_all

puts ""
puts "---------- Start seeding ----------"
puts ""

puts ""
puts "*********** Resetting all tables` sequences ***************"
puts ""
ActiveRecord::Base.connection.tables.each do |table|
  if table != "schema_migrations" && table != "ar_internal_metadata"
    puts "Resetting auto increment ID to 1 for table #{table}"
    ActiveRecord::Base.connection.execute("ALTER SEQUENCE #{table}_id_seq RESTART WITH 1")
    ActiveRecord::Base.connection.execute("UPDATE #{table} SET id=nextval('#{table}_id_seq')")
  end
end

puts ""
puts ""
puts "*********** Creating Users ***************"
puts ""
super_user = User.create(
  name: 'Admin',
  email: 'admin@example.ca',
  password: PASSWORD_ADMIN,
  admin: true
)
puts "User created --> Admin"

user1 = User.create(
  name: 'User One',
  email: 'user1@example.ca',
  password: PASSWORD
)
puts "User created --> User One"

user2 = User.create(
  name: 'User Two',
  email: 'user2@example.ca',
  password: PASSWORD
)
puts "User created --> User Two"

puts ""
puts "Created #{User.count} users"
puts ""

puts ""
puts "*********** Seed with artists data from cvs file ***************"
puts ""
require 'csv'
csv_text = File.read(Rails.root.join('lib', 'seeds', 'artist.csv'))
csv = CSV.parse(csv_text, :headers => true, :encoding => 'UTF-8')
csv.each do |row|
  x = Artist.new
  x.id = row['id']
  x.user_id = row['user_id']
  x.name = row['name']
  x.image = row['image']
  x.biography = row['biography']
  x.account_holder = row['account_holder']
  x.bank = row['bank']
  x.branch = row['branch']
  x.account = row['account']
  x.account_type = row['account_type']
  x.save
  puts "Artist saved --> #{x.image}"
  puts "Artist saved --> #{x.name}"
end

puts ""
puts "Created #{Artist.count} artists"
puts ""
puts ""
puts "*********** Instructions ***************"
puts ""
puts "Login as admin with #{super_user.email} and password of '#{PASSWORD_ADMIN}'!"
puts "Login as User1 with #{user1.email} and password of '#{PASSWORD}'!"
puts "Login as User2 with #{user2.email} and password of '#{PASSWORD}'!"

puts ""
puts ""
puts "---------- The End ----------"
puts ""
puts ""
