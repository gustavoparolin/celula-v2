json.extract! artist, :id, :user_id, :name, :image, :biography, :account_holder, :bank, :branch, :account, :account_type, :created_at, :updated_at
json.url artist_url(artist, format: :json)
