class Api::V1::ArtistsController < Api::ApplicationController
  before_action :authenticate_user!
  before_action :set_artist, only: [:show, :edit, :update, :destroy]

  # GET /artists
  # GET /artists.json
  def index
    @artists = Artist.all.order(name: :asc)
    render json: @artists
  end

  # GET /artists/1
  # GET /artists/1.json
  def show
    render json: @artist
  end

  # GET /artists/new
  def new
    @artist = Artist.new
  end

  # GET /artists/1/edit
  def edit
  end

  # POST /artists
  # POST /artists.json
  def create
    artist = Artist.new(artist_params)

    artist.save!
    render json: { id: artist.id }

  end

  # PATCH/PUT /artists/1
  # PATCH/PUT /artists/1.json
  def update
    if @artist.update(artist_params)
      render json: { message: 'artist updated', id: @artist.id }
    else
      render json: { message: 'artist NOT updated', error: @artist.errors.full_messages }
    end
  end

  # DELETE /artists/1
  # DELETE /artists/1.json
  def destroy
    if @artist.destroy
      head :ok
    else
      head :bad_request
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_artist
      @artist = Artist.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def artist_params
      params.require(:artist).permit(:user_id, :name, :image, :biography, :account_holder, :bank, :branch, :account, :account_type)
    end
end
