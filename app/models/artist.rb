class Artist < ApplicationRecord
  belongs_to :user, optional: true

  extend FriendlyId
  friendly_id :name, use: [:slugged, :history, :finders]

  mount_uploader :image, ImageUploader

  validates(
    :name, {
      presence: { message: 'Name must be provided' },
      uniqueness: { case_sensitive: false }
    }
  )

  before_save :squeeze

  private

  def squeeze
    self.name.squeeze!(' ')
  end

end
