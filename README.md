# ![picture](app/assets/images/logo-gustavo-parolin.png) Gustavo Parolin

# Celula - Version 2

## Project description

Rebuilding the application that manages the royalty payments of artists' digital music sales.

## See it running on heroku
https://parolin-celula.herokuapp.com/

* Login as admin with admin@example.ca and password of V@nc0uv3r
* Login as User1 with user1@example.ca and password of 123456

## Install:
```
Run the following commands in the project directory:

$ bundle
$ rails db:create
$ rails db:migrate
$ rails db:seed
$ foreman start

Open on localhost:5000
```

## Technologies used:
- Postgresql
- Ruby on Rails
