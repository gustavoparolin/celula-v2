require 'sidekiq/web'

Rails.application.routes.draw do
  namespace :admin do
    resources :users
    resources :announcements
    resources :notifications
    resources :services
    resources :artists

    root to: "users#index"
  end

  get '/privacy', to: 'home#privacy'
  get '/terms', to: 'home#terms'

  resources :notifications, only: [:index]
  resources :announcements, only: [:index]

  authenticate :user, lambda { |u| u.admin? } do
    mount Sidekiq::Web => '/sidekiq'
  end

  devise_for :users, controllers: { omniauth_callbacks: "users/omniauth_callbacks" }

  resources :artists

  root to: 'artists#index'

  namespace :api, defaults: {format: :json} do
    namespace :v1 do
      resources :artists, only: [:index, :show, :create, :destroy]
      resources :tokens, only: [:create]
    end
    # The following route will match any URL that hasn't been
    # matched already inside the :api namespace.
    # What makes this possible is putting a * in front of
    # the path (i.e. '#unmatched_route').
    # `match` can support multiple HTTP verbs at a time. They
    # must specified with the `via:` argument.
    match '*unmatched_route', to: 'application#not_found', via: :all
  end

end
